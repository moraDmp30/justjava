package com.example.android.justjava;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    private int quantity = 0;
    private boolean hasWhippedCream = false;
    private boolean hasChocolate = false;
    private String name = "";
    private int pricePerCup = 5;
    private int pricePerWhippedCream = 1;
    private int pricePerChocolate = 2;
    private int upperLimit = 100;
    private int lowerLimit = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        this.getNameFromEditText();
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.justjava_order) + " " + this.name);
        intent.putExtra(Intent.EXTRA_TEXT, this.createOrderSummary(this.calculatePrice()));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayQuantity() {
        TextView quantityTextView = findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + this.quantity);
    }


    /**
     * Increments the quantity in 1 unit and display.
     */
    public void increment(View view) {
        if (this.quantity < this.upperLimit) {
            this.quantity++;
            this.displayQuantity();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.upper_limit, new Object[]{this.upperLimit}), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Decrements the quantity in 1 unit and display.
     */
    public void decrement(View view) {
        if (this.quantity > this.lowerLimit) {
            this.quantity--;
            this.displayQuantity();
        } else {
            Toast.makeText(this, getString(R.string.lower_limit), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Calculates the price of the order based on the current quantity.
     *
     * @return the price
     */
    private int calculatePrice() {
        int price = this.pricePerCup;
        if (this.hasWhippedCream) {
            price += this.pricePerWhippedCream;
        }
        if (this.hasChocolate) {
            price += this.pricePerChocolate;
        }

        return price * this.quantity;
    }

    /**
     * Displays the order summary.
     *
     * @param price
     * @return String
     */
    private String createOrderSummary(int price) {
        return getString(R.string.name) + ": " + this.name + "\n" +
                getString(R.string.add_whipped_cream) + " " + (this.hasWhippedCream ? getString(R.string.yes) : getString(R.string.no)) + "\n" +
                getString(R.string.add_chocolate) + " " + (this.hasChocolate ? getString(R.string.yes) : getString(R.string.no)) + "\n" +
                getString(R.string.quantity) + ": " + this.quantity + "\n" +
                getString(R.string.total) + ": " + NumberFormat.getCurrencyInstance().format(price) + "\n" +
                getString(R.string.thankyou);
    }

    /**
     * Gets name from EditText and stores it on variable.
     */
    private void getNameFromEditText() {
        this.name = ((EditText) findViewById(R.id.name)).getText().toString();
    }

    /**
     * Is run when clicking the whipped cream checkbox.
     *
     * @param view
     */
    public void onWhippedCreamClick(View view) {
        this.hasWhippedCream = ((CheckBox) findViewById(R.id.whipped_cream_checkbox)).isChecked();
    }

    /**
     * Is run when clicking the chocolate checkbox.
     *
     * @param view
     */
    public void onChocolateClick(View view) {
        this.hasChocolate = ((CheckBox) findViewById(R.id.chocolate_checkbox)).isChecked();
    }
}
